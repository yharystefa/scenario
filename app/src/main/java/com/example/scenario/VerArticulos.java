package com.example.scenario;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.scenario.articulos.AdapterArticulos;
import com.example.scenario.articulos.CrearArticulo;
import com.example.scenario.articulos.ListArticulos;
import com.example.scenario.articulos.ProductosAdapter;
import com.example.scenario.domain.Constants;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class VerArticulos extends Fragment {

    List<ListArticulos> listArticulos;
    AdapterArticulos adaptador;
    RecyclerView recycler; //Adapta las tarjetas en la vista de la app
ProductosAdapter productosAdapter;
    public VerArticulos() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_ver_articulo, container, false);

        FloatingActionButton btnCrear = root.findViewById(R.id.btnCrear);
        btnCrear.setOnClickListener(view -> ((CardsItems) getActivity()).switchWindow(new CrearArticulo()));

        recycler = root.findViewById(R.id.recyclerId);
        recycler.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        productosAdapter = new ProductosAdapter();
        recycler.setAdapter(productosAdapter);

        Query query = FirebaseFirestore.getInstance().collection(Constants.PRODUCTS_COLLECTION_NAME);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                if (value != null && !value.isEmpty()) {
                    value.getDocuments().forEach(documentSnapshot -> {
                        Log.wtf("doc", documentSnapshot.getId());
                        try {
                            /*Aqui se deberian obtener los articulos, pero no está normalizada tu base de datos
                          :/ no tiene los tipos o nombres correctos
                            * */
                            ListArticulos articulos = new ListArticulos();
                            Log.wtf("item", documentSnapshot.get("codigo", String.class));
                            Log.wtf("item", documentSnapshot.get("nombre", String.class));
                            Log.wtf("item", documentSnapshot.get("descripción", String.class));
                            Log.wtf("item", documentSnapshot.get("precio").toString());
                            Log.wtf("item", documentSnapshot.getId());
                        } catch (Exception x) {
                            x.printStackTrace();
                        }
                        productosAdapter.añadirArticulo(new ListArticulos());
                    });

                }
            }
        });
        FirestoreRecyclerOptions<ListArticulos> options = new FirestoreRecyclerOptions.Builder<ListArticulos>()
                .setQuery(query, ListArticulos.class)
                .build();
        //Habría que utilizar el nuevo adaptador Products adapter en este caso y los metodos añadirArticulo y eliminarArticulo para mostrarlos
        //adaptador = new AdapterArticulos(options);
        //recycler.setAdapter(adaptador);

        FloatingActionButton btnSalir = root.findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(view -> {
            ((CardsItems) getActivity()).finish();
            System.exit(0);
        });

        return root;
    }
}


