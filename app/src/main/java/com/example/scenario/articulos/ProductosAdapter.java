package com.example.scenario.articulos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.scenario.CardsItems;
import com.example.scenario.R;
import com.example.scenario.VerArticulos;
import com.example.scenario.persistencia.DbProductos;

import java.util.ArrayList;
import java.util.List;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ProductosViewHolder> {
    List<ListArticulos> listaArticulos = new ArrayList<>();

    @NonNull
    @Override
    public ProductosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_articulos, null, false);
        return new ProductosViewHolder(view, view.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ProductosViewHolder holder, int position) {
        holder.asignarArticulos(listaArticulos.get(position));
    }

    public void añadirArticulo(ListArticulos articulo){
        listaArticulos.add(articulo);
        notifyDataSetChanged();
    }

    public void eliminarArticulo(ListArticulos articulo, Context context){
        DbProductos c = new DbProductos(context); // Instancia
        c.eliminarArticulo(Integer.toString(articulo.getCodigo()));
        Toast.makeText(context, "Se ha eliminado: " + articulo.getNombre(), Toast.LENGTH_SHORT).show();
        c.close();
        listaArticulos.remove(articulo);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return listaArticulos.size();
    }

     class ProductosViewHolder extends RecyclerView.ViewHolder {

        TextView nombrep;
        TextView codigop;
        TextView descripcion;
        TextView precio;
        Button btnEdit;
        Button btnRemove;
        Context context;

        public ProductosViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            nombrep = itemView.findViewById(R.id.nombre);
            codigop = itemView.findViewById(R.id.codigo);
            descripcion = itemView.findViewById(R.id.descripcion);
            precio = itemView.findViewById(R.id.precio);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnRemove = itemView.findViewById(R.id.btnRemove);
            this.context = context;
        }

        public void asignarArticulos(ListArticulos a) {
            nombrep.setText(a.getNombre().trim());
            codigop.setText(Integer.toString(a.getCodigo()).trim());
            descripcion.setText(a.getDescripcion().trim());
            precio.setText(Float.toString(a.getPrecio()).trim());
            btnEdit.setOnClickListener(view -> ((CardsItems) context).switchWindow(new ActualizarArticulo(a)));
            btnRemove.setOnClickListener(view -> {
                DbProductos c = new DbProductos(context); // Instancia
                c.eliminarArticulo(Integer.toString(a.getCodigo()));
                Toast.makeText(context, "Se ha eliminado: " + a.getNombre(), Toast.LENGTH_SHORT).show();
                c.close();
                ((CardsItems) context).switchWindow(new VerArticulos());
            });
        }
    }
}
