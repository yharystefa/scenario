package com.example.scenario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.scenario.persistencia.DbUsuarios;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

// Importar: Indica llamar las clases y archivos
// Extender: Indica usar la clase principal en las clases scundarias

public class MainActivity extends AppCompatActivity {
    // Dialog
    // private Dialog dialog;
    // private Button ShowDialog;
    EditText username, password;
    Button loginbtn, btnRegis; // Declaramos de forma global nuestra variable Button
    DbUsuarios DB;
    // ------ NEW ------
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide(); // Eliminamos la barrar superior

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);
        MaterialButton btnRegis = (MaterialButton) findViewById(R.id.btnRegis); // NEW

        //Acelera los test evitando ingresar la misma info si estamos en modo debug
        // IMPORTANTE eliminar antes de publicar
        if(BuildConfig.DEBUG){
            username.setText("Test");
            password.setText("test123");
        }

        // Conetext = La clase en la que estamos ubicados ahora mismo
        DB = new DbUsuarios(this);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString(); // Tomar los datos que agregó el usuario
                String pass = password.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(MainActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                else {
                    DB.checkcontrasena(user, pass,new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            Toast.makeText(MainActivity.this, "Verificando usuario", Toast.LENGTH_SHORT).show();
                            if(task.isSuccessful()){ // verificamos si se hizo correctamente la peticion al server
                                for (DocumentSnapshot doc : task.getResult()){
                                    if(doc.exists()){
                                        // Si el usuario ya existe.
                                        String contraenfirebase = doc.getString("contrasena");
                                        if (contraenfirebase.equals(pass)) {
                                            Toast.makeText(MainActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();
                                            Dialogo d = new Dialogo(MainActivity.this,"Aqui va el titulo","Hola gente", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(getApplicationContext(), CardsItems.class);
                                                    startActivity(intent);
                                                }
                                            });
                                        } else {
                                            Toast.makeText(MainActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else {
                                        //El usuario no existe.
                                        // Aquí lo insertaríamos.
                                    }
                                }
                            } else {
                                // Hubo un error en la conexión.
                            }
                        }
                    });
                }
            }

        });

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambiamos de layout/Vista
                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        });
    }
}


